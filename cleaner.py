import pymysql.cursors
import pandas as pd
from datetime import datetime, timedelta
import sys
import numpy as np
import time

# Connect to the database
connection = pymysql.connect(host='database-1.c5fcnokdjcfx.us-west-2.rds.amazonaws.com',
                             user='admin',
                             password='EqlSoDBtroOenGERLQHP',
                             db='SPOTIMYZE',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


with connection.cursor() as cursor:
    # Read all records
    sql = input('Your query here:')
    cursor.execute(sql)
    result = cursor.fetchall()
    
#cleaning and treating missing data 

#create dataframe
df = pd.DataFrame(result)

def cleaning(df):
    #filler for missing exit times
    df.loc[df['EXIT_TIME'] == 'N/A', 'EXIT_TIME'] = 0
    #string to time formats
    df['ENTRY_TIME'] = pd.to_datetime(df['ENTRY_TIME'])
    df['EXIT_TIME'] = pd.to_datetime(df['EXIT_TIME'])
    
    #secondary subsets for treating missing exit times with mean time period 
    df1 = df[(df['ENTRY_TIME'] < df['EXIT_TIME'])]
    x = np.mean(df1['EXIT_TIME'] - df1['ENTRY_TIME'])

    x = x.round('1s')
    df['ENTRY_TIME'] = df['ENTRY_TIME'].apply(lambda x:x.replace(microsecond = 0))
    df['EXIT_TIME'] = df['EXIT_TIME'].apply(lambda x:x.replace(microsecond = 0))
    #filling the actual df with mean times
    df.loc[df['EXIT_TIME'] < df['ENTRY_TIME'], 'EXIT_TIME'] = df.loc[df['EXIT_TIME'] < df['ENTRY_TIME'], 'ENTRY_TIME'] + x

    #'daily parker'is a transaction type flag in SURESPOT_TRANSACTION and for transaction sources such a Amano and Parkonect use
    # an appropriate flag for transient parkers
    #create subset dataframe to get daily parkers
    df2 = df.loc[df['TRANSACTION_TYPE'] == 'Daily Parker']
    df2 = df2[['ENTRY_TIME', 'EXIT_TIME', 'TRANSACTION_AMOUNT']]


    #get mean parking rate to fill missing values
    df3 = df2.loc[df2['TRANSACTION_AMOUNT'] != 'N/A']
    df3['TRANSACTION_AMOUNT'] = df3['TRANSACTION_AMOUNT'].astype('float')
    y = df3['TRANSACTION_AMOUNT'].mean()
    
    #fill missing values
    df2.loc[df2['TRANSACTION_AMOUNT'] == 'N/A', 'TRANSACTION_AMOUNT'] = y
    df2['TRANSACTION_AMOUNT'] = df2['TRANSACTION_AMOUNT'].astype('float')

    #removing microsecond data
    df2['ENTRY_TIME'] = df2['ENTRY_TIME'].apply(lambda x:x.replace(microsecond = 0))
    df2['EXIT_TIME'] = df2['EXIT_TIME'].apply(lambda x:x.replace(microsecond = 0))

    #remove seconds, making them zero
    df2['EXIT_TIME'] = df2['EXIT_TIME'].apply(lambda x:x.replace(second = 0))
    df2['ENTRY_TIME'] = df2['ENTRY_TIME'].apply(lambda x:x.replace(second = 0))

    clean_data = df2

    return clean_data


cleaning(df)