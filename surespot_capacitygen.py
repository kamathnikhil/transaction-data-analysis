import pymysql.cursors
import pandas as pd
from datetime import datetime, timedelta
import numpy as np
import time
import cleaner

df = cleaner.clean_data
# df = df.head(10000)

df = pd.read_csv("clean_data.csv")
df = df.head(10000)

def CapacityAnalysis(df):

    #convert df to lines
    lines = [",".join("{}".format(*t) for t in zip(row)) for _, row in df.iterrows()]
    lines.insert(0,'ENTRY_TIME,EXIT_TIME,TRANSACTION_AMOUNT ')

    #creating dict for minute-level breakdown
    idx = {}
    for i in range(len(lines[0][:-1].split(','))):
        idx[lines[0][:-1].split(',')[i]] = i

            # place holder for data: np array of array
    data = np.zeros((len(lines)-1, len(lines[0][:-1].split(','))), dtype = np.object)
    for i in range(1,len(lines)):
        x = i-1
        data_tmp = lines[i][:-1].split(',')
        for y in range(len(data_tmp)):
            data[x, y] = data_tmp[y]
    
        # process from string to usable objects
    min_date, max_date = datetime.max, datetime.min
    for i in data:
        # get idx of payment
        idx_cash = idx['TRANSACTION_AMOUNT']


        # get idx of entrance and exit
        idx_start = idx['ENTRY_TIME']
        idx_end = idx['EXIT_TIME']
        t1, t2 = datetime.strptime(i[idx_start], '%Y-%m-%d %H:%M:%S'), datetime.strptime(i[idx_end], '%Y-%m-%d %H:%M:%S')
        i[idx_start], i[idx_end] = t1, t2

        # global smallest date, largest date
        if min_date > i[idx_start]: min_date = i[idx_start]
        if min_date > i[idx_end]: min_date = i[idx_end]
        if max_date < i[idx_start]: max_date = i[idx_start]
        if max_date < i[idx_end]: max_date = i[idx_end]

        # add trans number and other shit if needs... 
        
        # get total number of minutes in the data from start to end
    tdelta = max_date - min_date
    range_days = int((tdelta).days)
    range_mins = int((tdelta).seconds/60)
    num_total_mins = (range_days*24*60 + range_mins)

    # place holder for final revenue
    # create vector to store the histogram
    # assume time is right continous . (0,1] types.

    idx_times = {}
    tmp_date = min_date
    for i in range(num_total_mins):
        tmp_date += timedelta(seconds=60)
        idx_times[tmp_date] = 0


    idx_occ = {}
    tmp_date = min_date
    for i in range(num_total_mins):
        tmp_date += timedelta(seconds=60)
        idx_occ[tmp_date] = 0
        
    
        # fill in result

    for i in data:
        # dt for this transaction
        exit = i[idx['EXIT_TIME']]
        enter = i[idx['ENTRY_TIME']]
        dt_ = exit - enter
        dt_days = dt_.days
        dt_secs = dt_.seconds
        dt_total_mins = int(dt_days*24*60 + dt_secs/60)

        # some data points have 0 dt, 0 payoff
        if (dt_total_mins == 0) or (i[idx['TRANSACTION_AMOUNT']] == 0):
            continue

        # fill the dict for the transaction
        current_fill_time = enter + timedelta(seconds=60)
        while current_fill_time <= exit:
            idx_occ[current_fill_time] += 1
            current_fill_time += timedelta(seconds=60)  

        return idx_occ


capacity = CapacityAnalysis(df)

def transformation(capacity):
    df = pd.DataFrame.from_dict(idx_occ, orient = 'index')
    df.reset_index(level=0, inplace=True)
    df = df.rename(columns={'index':'datetime',0:'Capacity_Per_Min'})
    df['datetime'] = df['datetime'].astype('str')
    df['datetime'] = pd.to_datetime(df['datetime'])
    df['Garage_Id'] = '1'
    df['Day_Of_Year'] = [d.date() for d in df['datetime']]
    df['Minute_Of_Day'] = [d.time() for d in df['datetime']]
    df['Transaction_Type'] = "transient"
    df["Source_Type"] = "Surespot"
    df.insert(0,'Id',np.nan)
    df = df.drop("datetime", axis = 1)
    df = df[['Id','Garage_Id','Day_Of_Year','Minute_Of_Day','Capacity_Per_Min','Transaction_Type','Source_Type']]
    #mention your output file path here. From here you will get your desired csv files from this function
    outpath = r'D:\Card_Activity'
    filename = "Surespot_Capacity_Analysis_"+(df['Day_Of_Year'].max().strftime("%m%d%Y"))+".csv"
    df.to_csv(outpath + "\\" + filename, index = False)
    return df
    
transformation(capacity)