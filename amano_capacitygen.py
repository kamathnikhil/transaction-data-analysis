import pymysql.cursors
import pandas as pd
from datetime import datetime, timedelta
import sys
import numpy as np
import time

# Connect to the database
connection = pymysql.connect(host='database-1.c5fcnokdjcfx.us-west-2.rds.amazonaws.com',
                             user='admin',
                             password='EqlSoDBtroOenGERLQHP',
                             db='SPOTIMYZE',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


with connection.cursor() as cursor:
    # Read all records
    sql = input('Your query here:')
    cursor.execute(sql)
    result = cursor.fetchall()
    
amano = pd.DataFrame(result)

def cleaning(amano):
    new_amano = amano[['ENTRY_TIME','EXIT_TIME','NET_AMOUNT']]
    df = new_amano[new_amano['ENTRY_TIME'] != '']
    df = df.reset_index(drop = True)
    df['ENTRY_TIME'] = pd.to_datetime(df['ENTRY_TIME'])
    df['EXIT_TIME'] = pd.to_datetime(df['EXIT_TIME'])
    df1 = df[(df['ENTRY_TIME'] < df['EXIT_TIME'])]
    x = np.mean(df1['EXIT_TIME'] - df1['ENTRY_TIME'])

    x = x.round('1s')
    
    df['ENTRY_TIME'] = df['ENTRY_TIME'].apply(lambda x:x.replace(microsecond = 0))
    df['EXIT_TIME'] = df['EXIT_TIME'].apply(lambda x:x.replace(microsecond = 0))
    
    #filling the actual df with mean times
    df.loc[df['EXIT_TIME'] < df['ENTRY_TIME'], 'EXIT_TIME'] = df.loc[df['EXIT_TIME'] < df['ENTRY_TIME'], 'ENTRY_TIME'] + x

    #'daily parker'is a transaction type flag in SURESPOT_TRANSACTION and for transaction sources such a Amano and Parkonect use
    # an appropriate flag for transient parkers


    #get mean parking rate to fill missing values
    df3 = df.loc[df['NET_AMOUNT'] != 'N/A']
    df3['NET_AMOUNT'] = df3['NET_AMOUNT'].astype('float')
    y = df3['NET_AMOUNT'].mean()
    
    #fill missing values
    df.loc[df['NET_AMOUNT'] == 'N/A', 'NET_AMOUNT'] = y
    df['NET_AMOUNT'] = df['NET_AMOUNT'].astype('float')

    #removing microsecond data
    df['ENTRY_TIME'] = df['ENTRY_TIME'].apply(lambda x:x.replace(microsecond = 0))
    df['EXIT_TIME'] = df['EXIT_TIME'].apply(lambda x:x.replace(microsecond = 0))

    #remove seconds, making them zero
    df['EXIT_TIME'] = df['EXIT_TIME'].apply(lambda x:x.replace(second = 0))
    df['ENTRY_TIME'] = df['ENTRY_TIME'].apply(lambda x:x.replace(second = 0))

    return df
    
clean_data = cleaning(amano)

def CapacityAnalysis(df):
    #convert df to lines
    lines = [",".join("{}".format(*t) for t in zip(row)) for _, row in clean_data.iterrows()]
    lines.insert(0,'ENTRY_TIME,EXIT_TIME,NET_AMOUNT')

    #creating dict for minute-level breakdown
    idx = {}
    for i in range(len(lines[0][:-1].split(','))):
        idx[lines[0].split(',')[i]] = i
        # place holder for data: np array of array
    data = np.zeros((len(lines)-1, len(lines[0][:-1].split(','))), dtype = np.object)
    for i in range(1,len(lines)):
        x = i-1
        data_tmp = lines[i][:].split(',')
        for y in range(len(data_tmp)):
            data[x, y] = data_tmp[y]
        # process from string to usable objects
    min_date, max_date = datetime.max, datetime.min
    for i in data:
        # get idx of payment
        idx_cash = idx['NET_AMOUNT']


        # get idx of entrance and exit
        idx_start = idx['ENTRY_TIME']
        idx_end = idx['EXIT_TIME']
        t1, t2 = datetime.strptime(i[idx_start], '%Y-%m-%d %H:%M:%S'), datetime.strptime(i[idx_end], '%Y-%m-%d %H:%M:%S')
        i[idx_start], i[idx_end] = t1, t2

        # global smallest date, largest date
        if min_date > i[idx_start]: min_date = i[idx_start]
        if min_date > i[idx_end]: min_date = i[idx_end]
        if max_date < i[idx_start]: max_date = i[idx_start]
        if max_date < i[idx_end]: max_date = i[idx_end]

            # add trans number and other shit if needs... 
    # get total number of minutes in the data from start to end
    tdelta = max_date - min_date
    range_days = int((tdelta).days)
    range_mins = int((tdelta).seconds/60)
    num_total_mins = (range_days*24*60 + range_mins)

    # place holder for final revenue
    # create vector to store the histogram
    # assume time is right continous . (0,1] types.

    idx_times = {}
    tmp_date = min_date
    for i in range(num_total_mins):
        tmp_date += timedelta(seconds=60)
        idx_times[tmp_date] = 0


    idx_occ = {}
    tmp_date = min_date
    for i in range(num_total_mins):
        tmp_date += timedelta(seconds=60)
        idx_occ[tmp_date] = 0
        
        # fill in result

    for i in data:
        # dt for this transaction
        exit = i[idx['EXIT_TIME']]
        enter = i[idx['ENTRY_TIME']]
        dt_ = exit - enter
        dt_days = dt_.days
        dt_secs = dt_.seconds
        dt_total_mins = int(dt_days*24*60 + dt_secs/60)

        # some data points have 0 dt, 0 payoff
        if (dt_total_mins == 0) or (i[idx['NET_AMOUNT']] == 0):
            continue


        # fill the dict for the transaction
        current_fill_time = enter + timedelta(seconds=60)
        while current_fill_time <= exit:
            idx_occ[current_fill_time] += 1
            current_fill_time += timedelta(seconds=60)  

    return idx_occ

capacity = CapacityAnalysis(clean_data)

def transformation(capacity):
    newdf = pd.DataFrame.from_dict(capacity, orient = 'index')
    newdf.reset_index(level=0, inplace=True)
    newdf = newdf.rename(columns={'index':'datetime',0:'Capacity_Per_Min'})
    newdf['datetime'] = newdf['datetime'].astype('str')
    newdf['datetime'] = pd.to_datetime(newdf['datetime'])
    newdf['Garage_Id'] = '1'
    newdf['Day_Of_Year'] = [d.date() for d in newdf['datetime']]
    newdf['Minute_Of_Day'] = [d.time() for d in newdf['datetime']]
    newdf['Transaction_Type'] = "transient"
    newdf["Source_Type"] = "Amano"
    newdf.insert(0,'Id',np.nan)
    newdf = newdf.drop("datetime", axis = 1)
    newdf = newdf[['Id','Garage_Id','Day_Of_Year','Minute_Of_Day','Capacity_Per_Min','Transaction_Type','Source_Type']]
    #mention your output file path here. From here you will get your desired csv files from this function
    outpath = r'D:\Card_Activity'
    filename = "Amano_Capacity_Analysis_"+(newdf['Day_Of_Year'].max().strftime("%m%d%Y"))+".csv"
    newdf.to_csv(outpath + "\\" + filename, index = False)
    return newdf

transformation(capacity)