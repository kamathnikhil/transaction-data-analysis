# Transaction Data Analysis

This repository consist of Amano, Parkonect and Surespot transaction data scripts i.e to calculate rate per min and capacity per min. The output from these scripts are inserted into AWS tables. These scripts are updated and can be used for incremental loads as well.

### Amano
amano_capacitygen.py
amano_ratecardgen.py

### Parkonect 
parkonect_capacitygen.y
parkonect_ratecardgen.py

### Surespot
cleaner.py
surespot_capacitygen.py
surespot_ratecardgen.py

For amano and parkonect clening function is added in the same script. For surespot cleaner.py file needs to run